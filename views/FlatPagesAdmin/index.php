<?php
/* @var $this FlatPagesAdminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Flatpages',
);

$this->menu=array(
    array('label'=>'Create Flatpages', 'url'=>array('create')),
    array('label'=>'Manage Flatpages', 'url'=>array('admin')),
);
?>

<h1>Flatpages</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_view',
)); ?>