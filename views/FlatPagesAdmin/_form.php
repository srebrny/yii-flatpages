<?php
/* @var $this FlatPagesAdminController */
/* @var $model Flatpages */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'flatpages-form',
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>120)); ?>
        <?php echo $form->error($model,'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'meta_key'); ?>
        <?php echo $form->textField($model,'meta_key',array('size'=>60,'maxlength'=>120)); ?>
        <?php echo $form->error($model,'meta_key'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'meta_desc'); ?>
        <?php echo $form->textField($model,'meta_desc',array('size'=>60,'maxlength'=>120)); ?>
        <?php echo $form->error($model,'meta_desc'); ?>
    </div>

    <div class="row">
    	<?php echo $form->labelEx($model,'content'); ?>
    	<?php $this->widget('application.extensions.redactorjs.Redactor', array( 'model' => $model, 'attribute' => 'content' )); ?>
	    <?php echo $form->error($model,'content'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'url'); ?>
        <?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>64)); ?>
        <?php echo $form->error($model,'url'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'layout'); ?>
        <?php echo $form->textField($model,'layout',array('size'=>60,'maxlength'=>64)); ?>
        <?php echo $form->error($model,'layout'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->