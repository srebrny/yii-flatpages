<?php
/* @var $this FlatPagesAdminController */
/* @var $model Flatpages */

$this->breadcrumbs=array(
    'Flatpages'=>array('index'),
    $model->title,
);

$this->menu=array(
    array('label'=>'List Flatpages', 'url'=>array('index')),
    array('label'=>'Create Flatpages', 'url'=>array('create')),
    array('label'=>'Update Flatpages', 'url'=>array('update', 'id'=>$model->id)),
    array('label'=>'Delete Flatpages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage Flatpages', 'url'=>array('admin')),
);
?>

<h1>View Flatpages #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'id',
        'title',
        'meta_key',
        'meta_desc',
        'content',
        'url',
        'layout',
    ),
)); ?>