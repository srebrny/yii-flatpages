<?php
/* @var $this FlatPagesAdminController */
/* @var $model Flatpages */

$this->breadcrumbs=array(
    'Flatpages'=>array('index'),
    'Create',
);

$this->menu=array(
    array('label'=>'List Flatpages', 'url'=>array('index')),
    array('label'=>'Manage Flatpages', 'url'=>array('admin')),
);
?>

<h1>Create Flatpages</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?> 