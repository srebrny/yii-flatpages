<?php
/* @var $this FlatPagesAdminController */
/* @var $model Flatpages */

$this->breadcrumbs=array(
    'Flatpages'=>array('index'),
    $model->title=>array('view','id'=>$model->id),
    'Update',
);

$this->menu=array(
    array('label'=>'List Flatpages', 'url'=>array('index')),
    array('label'=>'Create Flatpages', 'url'=>array('create')),
    array('label'=>'View Flatpages', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'Manage Flatpages', 'url'=>array('admin')),
);
?>

<h1>Update Flatpages <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?> 