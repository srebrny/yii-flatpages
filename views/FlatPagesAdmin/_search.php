<?php
/* @var $this FlatPagesAdminController */
/* @var $model Flatpages */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <div class="row">
        <?php echo $form->label($model,'id'); ?>
        <?php echo $form->textField($model,'id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'title'); ?>
        <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>120)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'meta_key'); ?>
        <?php echo $form->textField($model,'meta_key',array('size'=>60,'maxlength'=>120)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'meta_desc'); ?>
        <?php echo $form->textField($model,'meta_desc',array('size'=>60,'maxlength'=>120)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'content'); ?>
        <?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'url'); ?>
        <?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>64)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'layout'); ?>
        <?php echo $form->textField($model,'layout',array('size'=>60,'maxlength'=>64)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form --> 