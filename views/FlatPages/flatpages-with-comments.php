<div id="node-<?php echo $model->id; ?>" class="node node-blog node-promoted node-teaser"
	about="<?php echo $model->url; ?>" typeof="sioc:Post sioct:BlogPost">
	
	<div class="content clearfix">
		<div
			class="field field-name-body field-type-text-with-summary field-label-hidden">
			<div class="field-items">
				<div class="field-item even" property="content:encoded">
					<?php
						$this->beginWidget('CMarkdown', array('purifyOutput' => true));
						echo $model->content;
						$this->endWidget();
					?>
				</div>
			</div>
		</div>
		<div class="clearfix">
			<a name="komentarze"></a>
			<div class="comment-wrapper" id="comments" name="komentarze">
				<div id="comments-title">
					<div id="comments-title-left">
						<h2 class="title">Dodaj nowy komentarz</h2>
					</div>
					<div id="comments-title-right">
						<span class="counter">
							<fb:comments-count
								href="http://www.srebniak.pl/<?php echo $model->url;?>" />
							</fb:comments-count>
						</span>
					</div>
				</div>
				<div class="fb-comments" data-href="http://www.srebniak.pl/<?php echo $model->url;?>" data-num-posts="10" data-width="600"></div>
			</div>
		</div>
	</div>
</div>

