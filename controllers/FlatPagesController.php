<?php


Yii::import("common.components.Controller");
// Yii::import("common.modules.flatpages.models.Flatpages");

class FlatPagesController extends Controller
{

    public $defaultAction = 'Index';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Lists item.
     */
    public function actionIndex()
    {
        $url = Yii::app()->request->getQuery('url');
        if( empty($url)) {
            $url = 'index';
        }
        $m = $this->loadModelByUrl($url);
        $view = "index";

        if(!empty($m->type)) {
            $view = $m->type;
        }
        // var_dump(
        //     serialize(
        //         array(
        //             'class' => 'process',
        //              'assets' => array(
        //                  'css' => array('css/contact.css'=> CClientScript::POS_BEGIN ),
        //                  // 'js' => array(),
        //              )
        //         )
        //     )
        // );
        // This is a value of eg. serialize(array('class'=> 'process'));
        $params = @unserialize($m->params);
        if(!$params)
            $params = array();
        
        // var_dump($params['assets']);
        if ( array_key_exists('assets', $params) && !empty($params['assets']) ) {

            // We want add custom css and js for custom sites.
            // css array is key value pairs - index is file path - without assets prefix,
            // value is  CClientScript::POS_END or  CClientScript::POS_BEGIN
            if ( array_key_exists('css', $params['assets']) && !empty($params['assets']['css']) ) {
                foreach ($params['assets']['css'] as $file => $pos) {
                    // var_dump($file, $pos);
                    Yii::app()->clientScript->registerCssFile($this->themesResUrl.$file, 'screen');
                }
            }
            
            if ( array_key_exists('js', $params['assets']) && !empty($params['assets']['js']) ) {
                foreach ($params['assets']['js'] as $file => $pos)
                    Yii::app()->clientScript->registerScriptFile($this->themesResUrl.$file);
            }
        }


        $this->render('//flatpages/'.$view, array(
                'model'=>$m,
                'params' => $params,
            )
        );
    }
   
    /**
     * Returns the data model based on the url key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param string the url of the model to be loaded
     */
    public function loadModelByUrl($url)
    {
        $model=Flatpages::model()->findByAttributes(array('url'=> $url));
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     * @deprecated since 0.01
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='flatpages-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}