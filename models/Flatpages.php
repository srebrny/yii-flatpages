<?php

/**
 * This is the model class for table "{{flatpages}}".
 *
 * The followings are the available columns in table '{{flatpages}}':
 * @property integer $id
 * @property string $title
 * @property string $meta_key
 * @property string $meta_desc
 * @property string $content
 * @property string $url
 * @property string $layout
 */
class Flatpages extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Flatpages the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{flatpages}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, meta_key, meta_desc, content, url, layout', 'required'),
            array('title, meta_key, meta_desc', 'length', 'max'=>120),
            array('url, layout', 'length', 'max'=>64),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, meta_key, meta_desc, content, url, layout', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'meta_key' => 'Meta Key',
            'meta_desc' => 'Meta Desc',
            'content' => 'Content',
            'url' => 'Url',
            'layout' => 'Layout',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('meta_key',$this->meta_key,true);
        $criteria->compare('meta_desc',$this->meta_desc,true);
        $criteria->compare('content',$this->content,true);
        $criteria->compare('url',$this->url,true);
        $criteria->compare('layout',$this->layout,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
} 